use strict;
use File::Basename q/basename/;
use File::Copy;

my $dir = $ARGV[0];
my $cmd = $ARGV[1];
my $flat = !(defined($cmd) && length($cmd));

sub copy_file {
    my $file = shift;
    chomp $file;
    return unless $file =~ m/\.xml$/;
    my $source_subdir = 'texts/' . substr($file, 0, 3);
    my $target_subdir = 'texts/' . substr($file, 0, 4);
    mkdir $target_subdir unless -d $target_subdir;
    my $source = $flat ? "$dir/$file" : "$dir/$source_subdir/$file";
    my $target = "$target_subdir/$file";
    print STDERR "copying  $source to $target\n";
    copy($source, $target) or die "Could not copy $source to $target: $!";
}

if ($flat) {
    opendir(my $dh, $dir) || die "Can't open $dir: $!";
    while (my $file = readdir $dh) {
        copy_file($file);
    }
    closedir $dh;
}
else {
    open(my $fh, '-|', $cmd) or die "Can't run command $cmd: $!";
    while (my $file = <$fh>) {
        copy_file($file);
    }
    close $fh;
}