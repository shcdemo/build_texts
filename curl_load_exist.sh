#/bin/sh
file=$(basename "$1")
collection=$(echo $file | cut -c 1-3)

curl -s -f -H 'Content-Type: application/xml' \
     -T "$1" \
     --user shcadmin:@@PASSWORD@@ \
     "http://localhost:8080/exist/rest/db/apps/EarlyPrintTexts/texts/$collection/$file" \
     && echo "${file} successfully uploaded" \
     || echo "Curl failed with $? for ${file}"
