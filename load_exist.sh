#/bin/sh
if [ "$(uname)" == "Darwin" ]; then
    exist='/Applications/eXist-db.app/Contents/Resources/eXist-db/bin/client.sh'
else
    exist='/home/exist/eXist-db/bin/client.sh'
fi

file=$(basename "$1")
collection=$(expr substr $file 1 3)
${exist} -u shcadmin -P @@PASSWORD@@ --no-gui -m "/db/apps/EarlyPrintTexts/texts/$collection" -p "$1"
