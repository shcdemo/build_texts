# Text Loading Procedures

To add your own texts to the application, you must upload them using the
procedures described below.

## EarlyPrintTexts Package Creation

The first time you set up a new instance of the application, you must create
and install the EarlyPrintTexts package.  The package contains indexing
information and other metadata, as well as an empty texts collection that will
be populated as described in the next section.  To create the package, just
run ant like so:

    $ ant

Then use the eXist package manager to load the resulting .xar file from the
build/ directory.

## Load Texts

Before loading, texts should validate against their schema and every TEI
element should have an unique xml:id attribute.  Texts are maintained
elsewhere, but this project provides facilities to stage them for loading into
eXist.  First, there is a Perl script called stage.pl in the util/ directory
that will copy files into the appropriate subdirectories under the texts/
directory.  The subdirectories are named after the first three letters of the
XML files they contain, and they will be created if they do not already exist.

Before running stage.pl, first clean up any texts previously staged with the
following command or equivalent:

    $ rm -rf texts/*/

When passed a path to a single directory containing XML files, stage.pl copies
the files in that directory into the relevant local subdirectories, inferring
from each filename the correct subdirectory.  An example of running it this
way looks like:

    $ perl util/stage.pl ../staging
    copying  ../staging/A00456.xml to texts/A00/A00456.xml
    copying  ../staging/A00723.xml to texts/A00/A00723.xml
    . . .
    copying  ../staging/A04632_04.xml to texts/A04/A04632_04.xml
    copying  ../staging/A06583.xml to texts/A06/A06583.xml

When you pass two arguments to stage.pl on the command line, the first is a
path to a directory containing the same hierarchy of files that will be
produced locally.  In other words, that directory should already contain a
texts/ directory with further subdirectories under it named for the first
three letters of the filenames contained in them.

For the two-argument method, there must be a second argument on the command
line that will produce a list to standard output of the specific files
selected for staging.  An example of the two-argument version of stage.pl
looks like this:

    $ perl util/stage.pl ../eptexts ../eptexts/util/files_in_last_commit.sh
    copying  ../eptexts/texts/A01/A01520.xml to texts/A01/A01520.xml
    copying  ../eptexts/texts/A03/A03223_01.xml to texts/A03/A03223_01.xml
    . . .
    copying  ../eptexts/texts/A68/A68468.xml to texts/A68/A68468.xml
    copying  ../eptexts/texts/A77/A77567_04.xml to texts/A77/A77567_04.xml

Another example that will take all texts with a ".xml" extension in the
../eptexts directory hierarchy looks like this:

    $ perl util/stage.pl ../eptexts  "find ../eptexts/texts/ -name '*.xml' | xargs -n1 basename"

Once the files are staged under texts/, build the project with ant again:

    $ ant

but before you do, ensure that there is a file at ../admin.pwd containing only
the administrator password to the eXist server that is the target of the load
operation.  Once ant has completed, look in the build/ directory for the zip
file beginning with "texts_" and containing today's date in the filename.

Use whatever file transfer method is convenient to you to copy this zip file
to the server on which eXist is running.  Once it's there, unzip it and run
the load script it contains like so:

    $ unzip texts_20170707.zip
    $ cd texts_20170707
    $ ./load_all.sh > load.log 2>&1 &
    
This last command will load all the texts in the background to the local eXist
instance and leave results in load.log.  You may wish to tail that file
periodically to monitor the progress of the load.  When the load is complete,
check the log for errors and clean up the files used for the load.

## Update Metadata

Finally, after loading new or changed texts, in the user interface of the
application, navigate to the Browse window and click:

    Admin -> Update Document Metadata Index

You must be logged in as a member of the dba group to run the update.

